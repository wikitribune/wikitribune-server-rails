class CreateAuthorships < ActiveRecord::Migration[5.2]
  def change
    create_table :authorships do |t|
      t.references :user, foreign_key: true
      t.references :article, foreign_key: true
      t.string :comment

      t.datetime :date_created,  null: false
      t.datetime :date_modified, null: false
    end
  end
end
