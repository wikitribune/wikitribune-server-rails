class CreateArticles < ActiveRecord::Migration[5.2]
  def change
    say "Creating Articles…"
    create_table :articles do |t|
      #
      # https://schema.org/Article
      # but snake_cased for (perhaps misguided) consistency w/ the defaults
      # See config/initializers/jbuilder.rb to see how we serve camelCase.
      #

      t.string   :uuid
      t.index    :uuid, unique: true
      t.string   :slug
      t.index    :slug, unique: true
      t.string   :headline
      t.text     :article_body # from Article
      # t.text   :text         # from CreativeWorks

      t.column :status, :integer, default: Article.statuses[:draft]

      # Should Sections be tables in the database
      # t.string   :article_section

      t.datetime :date_created,  null: false
      t.datetime :date_modified, null: false
      t.datetime :date_published
    end
    # say "Indexing Articles…"
    # add_index :uuid
    # add_index :slug
  end
end
