class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      #
      # https://schema.org/Person
      # but snake_cased for (perhaps misguided) consistency w/ the defaults.
      # See config/initializers/jbuilder.rb to see how we serve camelCase.
      #

      t.string :name
      t.index :name, unique: true
      t.string :slug
      t.index :slug, unique: true
      t.string :email
      t.string :password_digest

      t.datetime :date_created,  null: false
      t.datetime :date_modified, null: false
    end
  end
end
