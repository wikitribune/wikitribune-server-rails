class CreateTopics < ActiveRecord::Migration[5.2]
  def change
    create_table :topics do |t|
      t.string :name
      t.string :slug

      t.datetime :date_created,  null: false
      t.datetime :date_modified, null: false
    end

    create_table :classifications do |t|
      t.references :article, foreign_key: true
      t.references :topic, foreign_key: true

      t.references :user # as author

      t.datetime :date_created,  null: false
      t.datetime :date_modified, null: false
    end
  end
end
