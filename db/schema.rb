# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2018_12_03_065411) do

  create_table "articles", force: :cascade do |t|
    t.string "uuid"
    t.string "slug"
    t.string "headline"
    t.text "article_body"
    t.integer "status", default: 1
    t.datetime "date_created", null: false
    t.datetime "date_modified", null: false
    t.datetime "date_published"
    t.index ["slug"], name: "index_articles_on_slug", unique: true
    t.index ["uuid"], name: "index_articles_on_uuid", unique: true
  end

  create_table "authorships", force: :cascade do |t|
    t.integer "user_id"
    t.integer "article_id"
    t.string "comment"
    t.datetime "date_created", null: false
    t.datetime "date_modified", null: false
    t.index ["article_id"], name: "index_authorships_on_article_id"
    t.index ["user_id"], name: "index_authorships_on_user_id"
  end

  create_table "classifications", force: :cascade do |t|
    t.integer "article_id"
    t.integer "topic_id"
    t.integer "user_id"
    t.datetime "date_created", null: false
    t.datetime "date_modified", null: false
    t.index ["article_id"], name: "index_classifications_on_article_id"
    t.index ["topic_id"], name: "index_classifications_on_topic_id"
    t.index ["user_id"], name: "index_classifications_on_user_id"
  end

  create_table "topics", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.datetime "date_created", null: false
    t.datetime "date_modified", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "slug"
    t.string "email"
    t.string "password_digest"
    t.datetime "date_created", null: false
    t.datetime "date_modified", null: false
    t.index ["name"], name: "index_users_on_name", unique: true
    t.index ["slug"], name: "index_users_on_slug", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.text "object", limit: 1073741823
    t.datetime "created_at"
    t.text "object_changes", limit: 1073741823
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

end
