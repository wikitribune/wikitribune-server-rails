# Sharp knives.  Also a slight code smell for a need to refacto as gem.

class String
  def to_slug
    # Specs we want (but don't have yet)
    # - /^
    #     [a-z][a-z0-9-]*[a-z0-9]  # three chars or more
    #     |                        # or
    #     [a-z][a-z0-9]?           # one or two characters
    #     |                        # or
    #                              # nothing
    #   $/
    # - lowercase
    # - slug-cased
    # - starts with a letter
    # - lettercentric <?> numbercentric
    # - implied `to_ascii_slug()` ; is that desirable?
    # - asciicentric <?> anglocentric
    # - RFC 3986 https://tools.ietf.org/html/rfc3986
    # - something to unit test \o/ \o/
    # I - the features of https://www.npmjs.com/package/slug
    # 404
    # II - StringEx impl, lacks <3 and ☢, unsure about rfc
    to_url
  end

  # def to_bool # tempting, but too dangerous, no? Let's be explicit for now.
  def param_to_bool
    return false if %w[❌ ❎ false no 0].include? self
    return true if %w[☑ ✅ ✓ ✔ 🗸 🗹 true yes 1].include? self

    ActiveRecord::Type::Boolean.new.deserialize(self)
  end
end