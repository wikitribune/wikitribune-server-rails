# If you change something to this file, run
# bin/bundle update
# bin/bundle install

source 'https://rubygems.org'

git_source(:github) { |repo| "https://github.com/#{repo}.git" }
# How to bundle the master from any github repo:
# gem 'rails', github: 'rails/rails'

ruby '~> 2.3'

# Let's get some rails for our news train.
gem 'rails', '~> 5.2.1'

# Should we go Relational, or Document?  <= I have no clue
# Pragmatically, use sqlite3 as the database for Active Record.
# This is great for development and testing.
# Production could use
# - MariaDb, MongoDb, or PostGres (recommended?)
# - flat files (+git)
gem 'sqlite3'

# Use Puma as the app server
gem 'puma', '~> 3.11'

# Use SCSS for stylesheets
# gem 'sass-rails', '~> 5.0'
# Use Uglifier as compressor for JavaScript assets
# gem 'uglifier', '>= 1.3.0'
# See https://github.com/rails/execjs#readme for more supported runtimes
# gem 'mini_racer', platforms: :ruby

# Use CoffeeScript for .coffee assets and views
# gem 'coffee-rails', '~> 4.2'
# Turbolinks makes navigating your web application faster.
# Read more: https://github.com/turbolinks/turbolinks
gem 'turbolinks', '~> 5'

# Use Redis adapter to run Action Cable in production
# gem 'redis', '~> 4.0'

# Go-to crypting tools.  (for ActiveModel has_secure_password)
# [audit required]
gem 'bcrypt', '~> 3.1.7'

# JSON Web Tokens for authentication
# Read more: https://jwt.io/
gem 'jwt', '~> 2.1.0'

# Slugify strings (among other things)
# /!. Don't use String#to_url from this, use String#to_slug.
# This does not slug unicode like ∞ or ♥.
# (javascript does it : https://www.npmjs.com/package/slug)
# If you know of a lib that does, let us know.
# There's also FriendlyId?
# Meanwhile, we've hooked in String#to_slug
# and we should use that internally.
# Read more: lib/core_ext/string.rb
gem 'stringex', '~> 2.8.5'

# We want YAML loading with symbolize_names: true
# and that is only available from version 3.0.0.
gem 'psych', '~> 3.0.0'

# Generate diff between two Strings
# 3.2.1 fixes a bug on Alpine (but they forgot to tag it in git)
# https://github.com/samg/diffy/pull/95
gem 'diffy', '~> 3.2.1'

# Build JSON APIs with ease.
# Read more: https://github.com/rails/jbuilder
# We're using this for now in our json views.  There's also Grape, to consider.
gem 'jbuilder', '~> 2.5'

# A good idea. The lib is young.
# gem 'schema_dot_org', '~> 1.6'

# API documentation generator
gem 'apipie-rails', '~> 0.5'

# Most Popular Revisioning
# https://github.com/paper-trail-gem/paper_trail
# Long-term (after publication) revisioning of articles should be done via git,
# once we implement the Archiving feature, perhaps.
gem 'paper_trail', '~> 10.0'

# Use ActiveStorage variant
# gem 'mini_magick', '~> 4.8'

# Use Capistrano for deployment
# gem 'capistrano-rails', group: :development

# Reduces boot times through caching; required in config/boot.rb
gem 'bootsnap', '>= 1.1.0', require: false

group :development, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a console.
  # This is very powerful when 'puts' or 'ap' are not suited. Breakpoints!
  gem 'byebug', platforms: [:mri, :mingw, :x64_mingw]
end

group :development do
  gem 'listen', '>= 3.0.5', '< 3.2'
  # Access an interactive console on exception pages or by calling 'console' anywhere in the code.
  gem 'web-console', '>= 3.3.0'
  # Spring speeds up development by keeping your application running in the background.
  # Read more: https://github.com/rails/spring
  gem 'spring'
  gem 'spring-watcher-listen', '~> 2.0.0'
end

group :test do
  # Adds support for Capybara system testing
  gem 'capybara', '>= 2.15'
  # ... and selenium driver. (which we're not using for now)
  # gem 'selenium-webdriver'
  # Easy installation and use of chromedriver to run system tests with Chrome
  # gem 'chromedriver-helper'

  # Cucumber is our Gherkin parser and Runner
  gem 'cucumber-rails', '~> 1.6', require: false
  # Allows cleaning the database between tests
  gem 'database_cleaner', '~> 1.7'
  # Nice expect() assertions we can use in our step defs
  gem 'rspec-rails', '~> 3.8'
  # Convert numbers into English words and vice versa
  gem 'numbers_in_words', '~> 0.4'
  # Print objects and json with colors and indentation
  gem 'awesome_print', '~> 1.8'

  # We probably will want these later
  # gem 'factory_girl_rails'
  # gem 'faker'
end

# Windows does not include zoneinfo files, so bundle the tzinfo-data gem
gem 'tzinfo-data', platforms: [:mingw, :mswin, :x64_mingw, :jruby]
