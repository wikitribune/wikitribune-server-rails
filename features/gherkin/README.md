This is where we should start our application design cycles.

Features are also a nice entry point for new contributors.
_Welcome!_


Features Summary
----------------

A generated [summary] of the features is available.



## Submodule

The features moved to another repository, [wikitribune-server-features](https://framagit.org/wikitribune/wikitribune-server-features).

We're using git submodules to include this other repository into this one,
at the path `features/gherkin/features`.


## Tips

- On _Rubymine_? `Ctrl` + `Click` on steps in the features
  to go to their step definition.  _VERY USEFUL_
- `bin/test` to run everything.
- Features or Scenarios tagged with `@wip` are ignored by `bin/test`.
- `bin/test --tags @wip` to run things tagged with `@wip`.
- Write Scenarios and implement them ONE by ONE.
- Have fun, and come on Discord, it's much more fun peer-coding these things!


### Given When Then And But

It does not matter which one you use in the steps or step defs.

It could be considered syntactic sugar.

We'd love to add `Since` to the list.  We tried, but it's not trivial.  Help!


### The `step` keyword

You can re-use other steps in step definitions, using `step`:

```gherkin
Then /^the response should contain (.+) things?$/ do |amount|
  expect(last_response_data[:things].size).to eq(amount.to_i)
end

Then /^I should be given (.+) things?$/ do |amount|
  step "the response should contain #{amount} things"
end
```


---

## issues

Sometimes, issues don't become features.
They become clutter, test cases tied to a codebase (rails, here).
We want them separately, because they're more tied to the
history of this codebase and make less sense from a generic perspective.

Separating gherkin issues and features helps keeping the features lean.


[summary]: https://framagit.org/wikitribune/wikitribune-server-features/blob/master/SUMMARY.md