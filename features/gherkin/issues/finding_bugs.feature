@bug
Feature: Finding bugs
  In order to expose bugs
  As a bug huntress
  I want to describe a scenario where they happen


Scenario: Not suggesting an article with suggestion=<falsey>
  Given I am the registered user named "Steven"
   When I post to "articles":
    """
    headline: Headline
    suggestion: false
    """
   Then there should be one article draft
    And there should still be no article suggestions
   When I post to "articles":
    """
    headline: Another Headline
    suggestion: 0
    """
   Then there should be two article drafts
    And there should still be no article suggestions
   When I post to "articles":
    """
    headline: Yet Another Headline
    suggestion: ❌
    """
   Then there should be three article drafts
    And there should still be no article suggestions
