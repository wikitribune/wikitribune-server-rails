# Analyze the current scenario, so we can turn printing off for Outlines

Before do |scenario|
  case scenario.source.last
  when Cucumber::Core::Ast::ExamplesTable::Row
    @scenario_name = scenario.scenario_outline.name
    @is_outline = true
  when Cucumber::Core::Ast::Scenario
    @scenario_name = scenario.name
    @is_outline = false
  else
    raise('Unhandled scenario class.  Check your Cucumber version?')
  end
end