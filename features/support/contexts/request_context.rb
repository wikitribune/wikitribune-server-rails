# Request contexts and tools

# Not sure if and why and how we should use modules to extend the World
# HELP APPRECIATED
# module RequestSteps
# end
# World(RequestSteps)

################################################################################

def version
  @version or 'v1'
end

def prefix
  "api/#{version}/"
end

def supported_formats
  %i[json jsonld]
end

################################################################################

def set_current_format(format)
  # See also get_response_data below before adding things here.
  raise('Unsupported format.') unless supported_formats.include? format

  @format = format
end

def format
  @format or :json
end

################################################################################

def request_api!(method, route, params={}, options={})
  request_api(method, route, params, options.merge(only_trying: false))
end

def request_api(method, route, params={}, options={})
  current_format = format

  # A (verbose!) trick to get `only_trying` and `format`
  # from either `params` or `options`.
  # /!. may collide with a legit parameter /!.
  #     - only_trying
  #     - format
  params = { only_trying: true }.merge(params)
  options = { only_trying: true }.merge(options)
  only_trying = params.delete(:only_trying) && options[:only_trying]
  format2   = params.delete(:format)
  format2 ||= options[:format]
  format2 ||= current_format
  # params.delete(:only_trying)
  # params.delete(:format)
  options.delete(:only_trying)
  options.delete(:format)
  ###

  options = options.merge(method: method, params: params)

  path = prefix + route + '.' + format2.to_s
  # can 'Authorization' be replaced by a constant?
  header 'Authorization', @my_auth if @my_auth
  @response = request path, options

  unless only_trying || last_response.successful?
    print_last_transaction
    fail "Request to API failed with code #{last_response.status} " +
         "(#{Rack::Utils::HTTP_STATUS_CODES[last_response.status]}).\n" +
         'An overview of the transaction is printed above.'
  end
  @response
end

def assert_that_response_exists
  raise('Request something first.') unless last_response
end

def get_response_data(response = nil, opts = { symbolize_names: true })
  unless response
    assert_that_response_exists
    response = last_response
  end

  # Perhaps the gem way involves Modules and
  # gems/rack-2.0.6/lib/rack/mock.rb
  if %i[json jsonld].include? format
    data = JSON.parse!(response.body, opts)
  # elsif %i[xml].include? format
  else
    raise("Format #{format} is not implemented.")
  end

  data
end

def last_response_data
  assert_that_response_exists
  get_response_data last_response
end

def print_response(response = nil)
  unless response
    assert_that_response_exists
    response = last_response
  end
  awesome_print(get_response_data(response, symbolize_names: false))
rescue JSON::ParserError
  puts(response.body) unless response.nil?
  puts '  ⇖⇖ warning: the reponse above could not be parsed as a data object.'
end

def print_last_response
  assert_that_response_exists
  print_response last_response
end

def print_last_request_headers
  headers = {}
  last_request.each_header do |header|
    headers[header[0]] = header[1] unless header[0].start_with?('action_', 'rack')
  end
  awesome_print headers
end

def print_last_transaction
  assert_that_response_exists
  puts "#{last_request.request_method} #{last_request.fullpath} WITH"
  # puts last_request.url
  print_last_request_headers
  awesome_print last_request.params
  puts last_response.status
  awesome_print last_response.headers
  print_last_response
end
