# Context for "I" semantics convention in Gherkin steps.

@my_self = nil # User
@my_name = nil # string
@my_pass = nil # string
@my_auth = nil # string JWT

################################################################################

def create_user!(name, pass)
  puts("🐵 Creating \"#{name}\"…") unless @is_outline
  # blackboxing for now
  @my_self = User.create!(name: name, email: get_email(name), password: pass)
  # ap(@my_self)
end

# get_ => fetch_ ? The idea is to get fresh data every time.

def get_user(name_or_where)
  # blackboxing for now
  return User.find_by(name: name_or_where) if name_or_where.is_a? String

  User.find_by(name_or_where)
end

################################################################################

def get_user!(name_or_where)
  get_user(name_or_where) || raise("No user found with for '#{name_or_where}'.")
end

def get_email(name)
  "#{name.to_slug}@test.wikitribune.org"
end

def get_password(name)
  name
end

# def myself ?
def get_myself
  # Instead of getting the possibly stale @my_self,
  # get a fresh copy from the database.
  @my_self = get_user!(@my_name)
end

def get_or_create_user(name, pass = name)
  user = get_user name
  user = create_user!(name, pass) unless user # ||==||
  user
end

def set_auth_token_from_response
  @my_auth = last_response_data[:auth_token]
end

def authenticate_user(name)
  puts("🔐 Authenticating as \"#{name}\"…") unless @is_outline
  params = { email: get_email(name), password: get_password(name) }
  request_api! :get, 'authenticate', params
  set_auth_token_from_response
end
