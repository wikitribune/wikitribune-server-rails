# Context for "that article" and "these_articles" in step defs.

@that_article = nil # Article
@these_articles = [] # Article[]

# How do we get these goodies?
# attr_reader :that_article
# attr_reader :these_articles
def that_article
  @that_article
end
def these_articles
  @these_articles
end

def get_article!(where)
  if where.is_a? String
    article_from_id = Article.find_with_effort where
    return article_from_id if article_from_id

    where = { headline: where }
  end
  Article.find_by! where
end

def set_that_article_from_response
  assert_that_response_exists
  if last_response.successful?
    @that_article = get_article!(id: last_response_data[:id])
  end

  self
end

def set_these_articles_from_response(from_collection=:all)
  # Note: :articles instead of 'articles'
  #       too much logic in there
  assert_that_response_exists
  if last_response.successful?
    @these_articles = []
    data = last_response_data
    if data.is_a? Array
      last_response_data.each do |v|
        @these_articles << get_article!(id: v[:id])
      end
    elsif data.is_a? Hash
      raise("No 'articles' key in response data") unless data[:articles]

      if data[:articles].is_a? Array
        data[:articles].each do |v|
          @these_articles << get_article!(id: v[:id])
        end
      elsif data[:articles].is_a? Hash
        raise("No '#{from_collection}' key in response data's articles") unless data[:articles][from_collection]

        data[:articles][from_collection].each do |v|
          @these_articles << get_article!(id: v[:id])
        end
      end
    end
  end

  self
end

def assert_that_article_exists
  raise('No "that article" is defined.') unless that_article

  self
end

def assert_these_articles_exist
  raise('No "these articles" is defined.') unless these_articles

  self
end