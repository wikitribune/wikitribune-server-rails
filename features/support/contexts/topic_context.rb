# Context for topics

@that_topic = nil

def get_topic!(where)
  Topic.find_by! where
end