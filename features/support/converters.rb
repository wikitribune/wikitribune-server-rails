# Handy converters for our step definitions' parameters.

# Mainstream usage recommends ParameterType
# which would be great to have along with regexes.

def convert_amount(amount)
  # /!. String#to_i is overriden and uses this (test env only)
  amount = 0 if %w[no none].include? amount
  amount = 1 if %w[a an].include? amount
  NumbersInWords.in_numbers(amount)
end

def from_yaml(yaml_pystring)
  Psych.load yaml_pystring, symbolize_names: true
end
