# AfterSuite
# After(:suite)
# There appears to be MANY people wanting this or a form of it.
# Apparently the Cucumber team is fine with the at_exit hack.
# Perhaps I'm just not finding the appropriate knowledge. Help!

# apt install fortunes cowsay

# ls /usr/share/cowsay/cows
# cowsay -l
cows = %i[apt beavis.zen bong bud-frogs bunny calvin cheese cock cower daemon default
dragon dragon-and-cow duck elephant elephant-in-snake eyes flaming-sheep
ghostbusters gnu head-in hellokitty kiss koala kosh luke-koala mech-and-cow
milk moofasa moose mutilated pony pony-smaller ren sheep skeleton snowman
stegosaurus stimpy suse three-eyes turkey turtle tux
unipony unipony-smaller vader vader-koala www]

eyes = [
  'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o', 'o',
  '0', '0', '0', '0', '0',
  '+',
  '.',
  'O', 'O', 'O', 'O', 'O',
  '~', '~', '~', '~', '~',
  'x',
  'ø', 'ø',
  'Ø', 'Ø',
  'o',
] # do add things here ; be creative! (funky unicode may yield broken ASCII art)


# Seriously? Sigh. If you find a better way to do this...
# I don't get how PHP managed to beat Ruby at this. Kudos, Behat.
should_show_fortune = true
scenarios_ran_count = 0
After do |s|
  should_show_fortune = false if s.failed?
  scenarios_ran_count += 1
end
AfterStep do |s|
  # Note: AfterStep hook is not triggered after Undefined steps. WTF
  # [link to issue required]
  # ap s
  # ap s.ok?
  # ap s.undefined?
  should_show_fortune = false unless s.ok?
end

at_exit do
  puts('') # newline
  # Positive reinforcement.  Related reading: Don't Shoot The Dog by Karen Pryor
  cmd = "fortune -a | cowsay -e '#{eyes.sample}#{eyes.sample}' -W 66 -f #{cows.sample}"
  system(cmd) if should_show_fortune && scenarios_ran_count > 0
end
