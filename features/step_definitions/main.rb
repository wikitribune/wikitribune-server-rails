# Blackboard for miscellaneous step definitions and possibly contexts.
# You can put your things in here until you figure out where they belong.

################################################################################
## SNIPPETS

Given /^some preconditions$/ do
end

When /^I do something$/ do
end

# Non-capturing parentheses group example
When /^I ?(?:now|maybe|perhaps)? do something$/ do
end

# Use String#to_i for amounts
When /^I do (.+)? things? twice$/ do |amount|
  puts("I did #{amount.to_i*2} things.")
end

Then /^things should be like this$/ do
end

When /^I grab "(.+)" to "(.+)"$/ do |title, route|
  request_api! :get, route, title: title
end

When /^I send "(.+)" to "(.+)"$/ do |title, route|
  request_api! :post, route, title: title
end


################################################################################

# ...

Then /^the response should contain (.+) (?:articles?|things?)$/ do |amount|
  assert_that_response_exists
  item = 'articles'
  collection = 'all'
  data = last_response_data
  if data.is_a? Array
    expect(data.size).to eq(amount.to_i)
  elsif data.is_a? Hash
    if data[item].is_a? Array
      expect(data[item].size).to eq(amount.to_i)
    elsif data[item].is_a? Hash
      expect(data[item][collection]).to be_a(Array)
      expect(data[item][collection].size).to eq(amount.to_i)
    end
  else
    fail("!")
  end
end
