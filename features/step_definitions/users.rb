
Given /^I am(?: now)? the registered user named "(.+)"$/ do |username|
  @my_name = username
  @my_pass = username
  @my_self = get_or_create_user(@my_name, @my_pass)
  @my_auth = authenticate_user(@my_name)
end

Given /^I am not logged in$/ do
  @my_name = nil
  @my_pass = nil
  @my_self = nil
  @my_auth = nil
end
