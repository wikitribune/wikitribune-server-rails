# Tentative time control steps.

# Ideas
# A PropertyType for datetimes, and another for durations.
# Here, try to control Rails' kernel time and perhaps server time?

def set_time(moment)
  # TODO - HELP
end

def pass_time(duration)
  # TODO - HELP
end

Given /^the year is (.+)$/ do |year|
  set_time(year: year)
end

Then /^(.+) years? pass(?:es)?$/ do |years|
  pass_time(years: years.to_i)
end

Then /^Time, the healer, passes for (.+) years?$/ do |years|
  pass_time(years:  years.to_i)
end