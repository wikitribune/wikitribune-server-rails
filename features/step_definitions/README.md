# Step definitions

For each Gherkin step, we have a step definition that actually executes code.


## Guidelines

Step defs should be **short** and **sweet**.

Don't hesitate about editing other people's code, changing names,
moving things around. Gardening, sweeping, commenting…

The codebase should be a living thing, just like our documentation is.


## Tips

Using _Rubymine_?
You can set up `bin/test` as a Runner and get links straight to the code
in stacktraces. Typing `Shift + F10` runs it in a window you can pop out
and put on another screen for example.


## Regex

> I don't understand these lovecraftesque regular expressions! 

Just download and/or print this :
https://agileforall.com/wp-content/uploads/2011/08/Cucumber-Regular-Expressions-Cheat-Sheet.pdf

There's about 15 concepts to grasp. (there's more, but we probably won't be using them)


## Notes

We're using _rspec_ for the assertions.

> We're not overly fond of the syntax.
> If you have suggestions for better assertions, we're all ears.

Meanwhile, here's the `expect()` [cheatsheet](https://relishapp.com/rspec/rspec-expectations/docs/built-in-matchers).

---

The architecture in here is … in flux.
If you have ideas or experience, please share them.

There should probably be no files (maybe one) in this directory.
We can move files around safely, don't hesitate on experimenting and suggesting
new file trees!

---

Let's try to ensure we can still go full-blackbox later on,
that means without any direct access to the kernel and database.