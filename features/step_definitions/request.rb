# Relies on support/contexts/request_context.rb
# These steps defs should be used with caution,
# as they impair readability of the features.
# Still, they are useful as sugar during development
# and to expose edge-cases where writing additional steps
# would only be growing the clutter.
# Perhaps we'll use them more liberally for lower-level specs.

When /^I( try to|) (?:get|GET) "(.*)"$/ do |try, route|
  params = { only_trying: try != '' }
  request_api :get, route, params
end

When /^I( try to|) (?:get|GET) "(.*)"(?: with(?: param(?:eter)?s?)?)? *:$/ do |try, route, pystring|
  params = { only_trying: try != '' }.merge(from_yaml(pystring))
  request_api :get, route, params
end

When /^I( try to|) (?:post|POST) to "(.*)" *:$/ do |try, route, pystring|
  params = { only_trying: try != '' }.merge(from_yaml(pystring))
  request_api :post, route, params
end

When /^I( try to|) (?:put|PUT) in "(.*)" *:$/ do |try, route, pystring|
  params = { only_trying: try != '' }.merge(from_yaml(pystring))
  request_api :put, route, params
end
