# Relies on support/contexts/request_context.rb

Then /^I should (succeed|fail)(?: \(.+\))?$/ do |act|
  assert_that_response_exists
  unless last_response.successful? == (act == 'succeed')
    print_last_transaction
    fail("Expected request to #{act} and that was not the case.")
  end
end

Then /^I should (succeed|fail) with code (.+)(?: \(.+\))?$/ do |act, code|
  assert_that_response_exists
  expect(last_response.successful?).to eq(act == 'succeed')
  expect(last_response.status).to eq(code.to_i)
end

Then /^(?:the|my) request should be (?:accepted|successful)(?: \(.+\))?$/ do
  assert_that_response_exists
  expect(last_response.successful?).to be_truthy
end

Then /^(?:the|my) request should be denied(?: \(.+\))?$/ do
  assert_that_response_exists
  expect(last_response.successful?).to be_falsey
end

Then /^(?:the|my) response(?:'s)?(?: status)? code should be ([0-9]+)(?: \(.+\))?$/ do |code|
  assert_that_response_exists
  expect(last_response.status).to eq(code.to_i)
end

Then /^I (?:dump|print) th(?:e|at|is) response$/ do
  print_last_response
end

Then /^I (?:dump|print) th(?:e|at|is) (?:whole|last|latest|recent)? ?transaction$/ do
  print_last_transaction
end

Then /^I set the current API format to (.+)$/ do |human_format|
  format_as_atom = { 'JSON' => :json, 'JSON-LD' => :jsonld }
  set_current_format format_as_atom[human_format]
end
