# Steps for blackbox assertions directly into the database of articles.

# We do a lot of `amount.to_i`
# investigate how to DRY
# -> can we create a convention around the `amount` variable name?
# -> see users_assertions.rb

################################################################################

Then /^there should(?: still)? be (.+) article suggestions?(?: in the database)?$/ do |amount|
  expect(ArticleSuggestion.count).to eq(amount.to_i)
end

Then /^there should(?: still)? be (.+) article drafts?(?: in the database)?$/ do |amount|
  expect(ArticleDraft.count).to eq(amount.to_i)
end

Then /^there should(?: still)? be (.+) (?:published article|article publication)s?(?: in the database)?$/ do |amount|
  expect(ArticlePublication.count).to eq(amount.to_i)
end

Then /^there should be (.+) article drafts? titled "(.*)"$/ do |amount, title|
  expect(ArticleDraft.where(headline: title).count).to eq(amount.to_i)
end

Then /^there should be (.+) article drafts? titled "(.*)" containing:$/ do |amount, title, content|
  expect(ArticleDraft.where(headline: title, article_body: content).count).to eq(amount.to_i)
end

Then /^there should be (.+) (?:published article|article publication)s? titled "(.*)" containing:$/ do |amount, title, content|
  expect(ArticlePublication.where(headline: title, article_body: content).count).to eq(amount.to_i)
end

Then /^there should be (.+) (?:published article|article publication)s? titled "(.*)"$/ do |amount, title|
  expect(ArticlePublication.where(headline: title).count).to eq(amount.to_i)
end

Then /^the user "(.+)" should have (.+) articles? published in their name$/ do |username, amount|
  expect(get_user!(username).published_articles_count).to eq(amount.to_i)
end

Then /^I(?: now| still)? should have (.+) articles? published in my name$/ do |amount|
  expect(get_myself.published_articles_count).to eq(amount.to_i)
end

Then /^the article "(.+)" should(?: also)?(?: still)?( not|) be in the topic "(.+)"$/ do |article, nope, topic|
  @that_article = get_article! article
  @that_topic = get_topic! name: topic
  found = @that_article.topics.where(name: @that_topic.name).count >= 1
  validates = nope.empty? ? found : !found
  unless validates
    puts 'Topics found:'; awesome_print @that_article.topics
    puts "but expected to#{nope} find topic:"; awesome_print @that_topic
    fail("Topic `#{@that_topic.name}' was#{' not' if nope.empty?} found in article `#{@that_article.headline}'.")
  end
end