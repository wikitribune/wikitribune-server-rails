Given /^a new instance of wikitribu[tn]e without fixtures$/ do
  # nothing is cool
end

# Note: I18N and L10N sometimes are desirable as fixtures.

################################################################################
# ARTICLES

Given /^there already is an article draft titled "(.*)"(?: in the database)?$/ do |title|
  @that_article = Article.create!(headline: title)
end

Given /^there already is an article draft titled "(.*)" by "(.+)"(?: in the database)?$/ do |title, author|
  @that_article = Article.create!(headline: title, author: get_or_create_user(author)).publish!
end

Given /^there already is a published article titled "(.*)"(?: in the database)?$/ do |title|
  @that_article = Article.create!(headline: title).publish!
end

Given /^there already is an article like this(?: in the database)?:?$/ do |pystring|
  params = from_yaml(pystring)
  @that_article = Article.create!(params)
end

################################################################################
# TOPICS

Given /^there already is a topic named "(.*)"(?: in the database)?$/ do |name|
  @that_topic = Topic.create!(name: name)
end