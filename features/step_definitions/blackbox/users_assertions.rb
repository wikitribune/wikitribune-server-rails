# Assertions about the state of Users.

# Workbench for transforms experiments. WiP

# HELP
# In order to get rid of amount = convert_count amount
#
# I - ParameterTypes and Regexes
# We want converters|transforms AND regexes.
# I can't get this to work, much less play along with regexes.
# Hitting https://github.com/cucumber/cucumber/issues/329#issuecomment-361783591
# ParameterType(
#   name:        'amount',
#   regexp:      /^.+$/,
#   # regexp:      /^(.+)$/,
#   type:        Float,
#   transformer: ->(s) { ap('s') ; ap(s) ; convert_count(s) }
# )

# II -
# We can replace all the assignations by calls to String#to_i
# This works. Reduces clutter, but leaves some.  Convention is preferable.
# Also, this is perhaps an anti-pattern ; a very bad idea.
class String
  # I - Yes
  def to_i
    convert_amount(self)
  end
  # II - No
  # def to_i
  #   to_f.to_i
  # end
  # def to_f
  #   convert_count(self)
  # end
  # III - Yes ... Meh
  def natural
    floating.to_i
  end
  def floating
    convert_amount(self)
  end
end

# III - ???
# class Then
#   def call(a, &b)
#     … &b.keys …
#     *voodoo voodoo*
#     super.call(a, &b)
#   end
# end

# IV - BeforeStep
# Another nope.

# Conclusion - I need to koans, and


# Then("there should be {amount} users in the database") do |amount|
# Witnessing the above failing, checked versions. We should have it, I think.
# Then /^there should(?: still)? be (?<amount>.+) users?(?: in the database)?$/ do |amount|
Then /^there should(?: still)? be (.+) users?(?: in the database)?$/ do |amount|
# Then "there should be {amount} users in the database" do |amount|
#   ap 'amount'
#   ap amount
#   amount = convert_count amount
  expect(User.count).to eq(convert_amount(amount))
  expect(User.count).to eq(amount.to_i)
  expect(User.count).to eq(amount.natural)
end
