When /^I( try to|) register as ?(?:the|a)?(?: new)? user "(.+)" with password "(.*)"$/ do |try, name, pass|
  try = try == ' try to'
  params = {
    email: get_email(name), # fake it until we make emails optional
    name: name,
    password: pass
  }
  request_api :post, 'users', params, only_trying: try
end

When /^I( try to|) authenticate as "(.+)" with password "(.*)"$/ do |try, name, pass|
  try = try == ' try to'
  params = {
    email: get_email(name), # fake it until we make emails optional
    name: name,
    password: pass
  }
  request_api :get, 'authenticate', params, only_trying: try
  set_auth_token_from_response if last_response.successful?
end
