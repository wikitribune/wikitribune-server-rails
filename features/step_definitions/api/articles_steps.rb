When /^I( try to|) submit a new article draft titled "(.+)" with the following content:$/ do |try, title, pystring|
  try = try == ' try to'
  params = {
    headline: title,
    article_body: pystring
  }
  request_api :post, 'articles', params, only_trying: try
  set_that_article_from_response
end

When /^I( try to|) submit a new article draft titled "(.+)" with(?: no|out) content$/ do |try, title|
  try = try == ' try to'
  request_api :post, 'articles', headline: title, only_trying: try
  set_that_article_from_response
end

When /^I( try to|) suggest(?: again)? a new article titled "(.+)"$/ do |try, title|
  request_api :post, 'articles',
              only_trying: try != '',
              suggestion: true,
              headline: title
  set_that_article_from_response
end

When /^I( try to|) edit the(?: published)? article(?: draft)? titled "(.+)" with the following content:$/ do |try, title, pystring|
  try = try == ' try to'
  target_article = get_article! headline: title
  params = {
    headline: title,
    article_body: pystring
  }
  request_api :put, "articles/#{target_article.id}", params, only_trying: try
  set_that_article_from_response
end

When /^I( try to|) add the article "(.+)" to the topic "(.+)"$/ do |try, article, topic|
  @that_article = get_article! article
  @that_topic = get_topic! name: topic
  request_api :post, 'classification',
              only_trying: try == ' try to',
              article: @that_article.id,
              topic: @that_topic.id
end

When /^I( try to|) remove the article "(.+)" from the topic "(.+)"$/ do |try, article, topic|
  @that_article = get_article! article
  @that_topic = get_topic! name: topic
  request_api :delete, 'classification',
              only_trying: try == ' try to',
              article: @that_article.id,
              topic: @that_topic.id
end

When /^I( try to|) publish that article$/ do |try|
  try = try == ' try to'
  assert_that_article_exists
  request_api :post, "articles/#{@that_article.id}/publish", only_trying: try
  set_that_article_from_response
end

When /^I( try to|) request the article (?:with|of) id "?(.+?)"?$/ do |try, id|
  try = try == ' try to'
  request_api :get, "articles/#{id}", only_trying: try
  set_that_article_from_response
end

When /^I request the most recently published articles$/ do
  request_api! :get, 'articles'
  set_these_articles_from_response
end

When /^I request the (.+) most recently published articles$/ do |amount|
  request_api! :get, 'articles', limit: amount.to_i
  set_these_articles_from_response
end

When /^I request the articles of the topic "(.+)"$/ do |topic|
  request_api! :get, "topics/#{topic.to_slug}"
  set_these_articles_from_response :all
end

################################################################################

Then /^that article's (?:title|headline) should be "(.+)"$/ do |headline|
  assert_that_article_exists
  expect(that_article.headline).to eq(headline)
end

Then /^I should be given (.+) articles?$/ do |amount|
  assert_these_articles_exist

  expect(these_articles).to be_a(Array)
  expect(these_articles.size).to eq(amount.to_i)
end

Then /^that article's diff with its previous version should be:$/ do |pystring|
  assert_that_article_exists
  request_api! :get, "articles/#{that_article.id}/diff"
  #print_last_response
  # For clarity in the features, we only compare lines with changes
  lines = last_response_data.select { |line| line.start_with? '+', '-' }
  expect(lines.join('')).to eq(pystring + "\n")
end

Then /^th[eo](?:se)? articles should be sorted by publication date, most recent first$/ do
  assert_these_articles_exist
  sorted_articles = these_articles.sort_by { |a| a[:date_created] }.reverse
  unless sorted_articles == these_articles
    puts 'EXPECTED'
    awesome_print sorted_articles
    puts 'GOT'
    awesome_print these_articles
    fail('Not sorted correctly. Inspect why above.')
  end
end
