
Framework Choice
================

![Stack Doubt — Commitstrip](https://www.commitstrip.com/wp-content/uploads/2018/10/Strip-Le-doute-eternel-de-la-slack-650-finalenglish.jpg)


### Wordpress

Wikitribune's Pilot is made using Wordpress.

Pros:

- PHP.
- Already a blog engine, close enough to a journal.
- The Wordpress team might embrace upstream the idea of a massively collaborative blog engine.
- Provides a shiny frontend and moderation features.
- Lots of plugins.
  - Including for [ActivityPub](https://wordpress.org/plugins/activitypub/)!
- Lots of developers ([so much](https://www.similartech.com/compare/ruby-on-rails-vs-wordpress)).
- Now with [an API](https://v2.wp-api.org/).



Cons:

- PHP.
- Hard to test.
- Beacon for automated attacks ([vulnerabilities](https://wpvulndb.com/)).
- Extending will become more and more complex.
- API appears somewhat complicated to extend.
- No battle-tested Model-View-Controller pattern.
- Tied to MySQL or MariaDb. (mild con)


### Symfony

Pros:

- PHP.
- Symfony Components.
- Lots of bundles.
- Lots of developers.
- Fast and lightweight.
- Excellent testing tools and suites.
- Wikitribune's dev team knows PHP.
- Wikitribune's Pilot code is in PHP.
- > I've been using it for 10+ years.


Cons:

- PHP.
- Steep learning curve.


### Ruby on Rails

Pros:

- Ruby.
- Lots of gems.
- Excellent testing tools and suites.
- Beautiful, idiomatic code.
- Lots of people with keyboards know Ruby.
- [Discourse](https://www.discourse.org/) can be forked as well.
- > I want to do more Ruby.


Cons:

- Ruby.
- Slow. (relies heavily on cache)


### NodeJs

Cons:

- Javascript.
- JAVASCRIPT.
- Poor testing tools and suites.

_Please edit this section — I just can't._


### Other frameworks

Cons:

- Poor or absent testing tools and suites.

-----

_Advertisement_: [Elixir](https://elixir-lang.org/) is awesome.


-----

Where are the pixels?
=====================

Have you ever heard of [API-first development](https://www.qwant.com/?q=api+first+development)?

Modern, community-driven projects benefit most from this kind of agile methodology.
It's super-hard (impossible?) to make an _UI_ that will satisfy everyone.

How do you conciliate:

- Desktop users
    - I want a widget on my desktop, showing the latest articles
    - I want pristine support for _&lt;insert desktop browser here&gt;_
    - I want a browser plugin for _&lt;insert desktop browser here&gt;_
- Mobile users
    - I want a lightweight, mobile-first website, for _&lt;insert mobile browser here&gt;_ 
    - I want a mobile app for _&lt;insert mobile OS here&gt;_
- Sight-impaired users
    - I need compatibility with my special browser and tools
    - I need a theme without any _&lt;insert color here&gt;_
    - I need bigger fonts, line heights and margins
- Movement-impaired users
    - I need compatibility with my special browser and tools
- Most people
    - I want it to be beautiful and responsive
    - I want WYSIWYG.
    - I want a _&lt;weekly|…&gt;_ newsletter
- Robots
    - I want a Command-Line Interface for my news
    - I want my server to display the latest articles when I log in through SSH
- Developers
    - I want the frontend to use _&lt;insert frontend tech here&gt;_
    - I want to make my own client, for my special needs
- Security researchers
    - I want a frontend without any _&lt;insert javascript here&gt;_


With API-first development, you just make a _REST_ (or _Soap_, or whatever) _API_,
and let the community make their own User Interfaces, paint their own pixels, however they want, in other repositories.

It also allows you to focus on the business logic and leave the pixel painting and user experience crafting to other repositories.
This separation of concerns:
- yields optimal flexibility (all UIs may be made)
- facilitates developer onboarding (provided the API doc is well-made)
- allows developers to focus (and keep their sanity)
- allows code repositories to remain manageable in size and issues

-----

Speaking of developers' mental health and happiness, this leads us to… 


Story-Driven Development
========================

If you do not know what _SDD_ is, [Cucumber.io](https://cucumber.io/)
has great explanations about it, and why it's so powerful.
They're making money off of it, so it's to take with a grain of salt, but still…
Developers who embraced it are dithyrambic about it.

It allows non-technical people to contribute and it makes bug reporting much easier.

It looks like this:

```gherkin
Feature: Writing an article
  As a reporter
  In order to share important facts
  I want to write an article about them

Scenario: Writing a new article
  Given I am the registered user named "Jimmy Wales"
   When I submit a new article stub titled "Making WikiTRIBUNE" with the following content:
"""
Hello world!  I'm a guy with a keyboard.  Let's make a journal together!
"""
   Then there should be one article stub in the database
    And there should be an article stub titled "Making WikiTRIBUNE"
   When I publish that article
   Then there should be zero article stubs in the database
    And there should be a published article titled "Making WikiTRIBUNE"
    And the user "Jimmy Wales" should have one article published in their name
```

Writing specifications like this:
- helps defining the semantics of the app
- reduces the cognitive load of developers
  - when adding new code or refactoring
  - especially for developers that are new to the codebase
- is great for bug reporting
- engages non-technical users to contribute
- provides an anchor for UX debates


> If it's so great, why aren't we all doing it?

There are multiple factors:
- it's not taught at school, I'm told
- various cognitive biases
- sometimes it's hard to mock third-parties
- writing step definitions usually requires understanding regular expressions
- _"we don't have time for tests"_
- _"we trust our coding skills"_


You're mistaken
===============

_It happens more than we'd like._

Please submit a Pull Request or open an Issue,
so we can talk about it and right the wrongs.


-----

Acronyms
========

> Real geniuses don't define acronyms ; they understand them genetically.


 Acronym | Meaning
---------|----------------------------------------------------------------------
API      | Application Public Interface
CLI      | Command-Line Interface
EAFP     | Easier to Ask for Forgiveness than Permission
SSH      | Secure Shell
SDD      | Story-Driven Development
UI       | User Interface
UX       | User eXperience
WYSIWYG  | What You See Is What You Get




