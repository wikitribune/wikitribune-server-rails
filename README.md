Wikitribune
===========

> Come collaborate with us, because facts really do matter.


What is this?
-------------

This is a draft of what could be specifications of a REST API for Wikitribune, using:

- Behavior-Driven Development (in [Gherkin](https://en.wikipedia.org/wiki/Cucumber_(software)#Gherkin_language))
- Ruby on Rails ([Why?](FRAMEWORK_RATIONALE.md))
- [API Application](https://guides.rubyonrails.org/api_app.html) Pattern
- [JWT](https://jwt.io/) Authentication. (for now)


Can I help?
-----------

Yes, please.
We're very new to Ruby and Rails, and we need help and tips.


Installation
------------

#### Debian / Ubuntu

    # apt install sqlite3 libsqlite3-dev
    # apt install g++ ruby ruby-dev

Optionally, _but it's worth it_

    # apt install fortunes cowsay


#### Mac

> homebrew


#### Everyone
    
    # gem install bundler
    $ git clone https://framagit.org/wikitribune/wikitribune-server-rails.git --recurse-submodules
    $ cd wikitribune-server-rails
    $ bin/install
    $ bin/test


#### Troubleshooting

If the directory `features/gherkin/features` is non-existent or empty,
you probably cloned without the submodules.

    $ git submodule update --init


Onboarding
----------

0. Fork.
1. Start by reading [the features](features/gherkin).
2. Run them with `bin/test`.
3. Write _one_ Scenario.
4. Is step 2. running green?
    - Yes. Merge-Request it.
    - No.  Can you make it green?
        - Yes. _Carry on, thank you very much! We're all in this together!_
        - No.  _Merge-Request it using the word `#feature` or `#issue`._
        - Perhaps; I need help.  _There's [an app](https://discord.gg/zxhDhxe) for that!_
5. Repeat from 3.


Development
-----------

    $ bin/rails server

You can then browse the generated documentation at http://localhost:3000/apipie

That's not very useful right now.
In practice, we use the tests.


Test
----

Testing is done through running the Gherkin Scenarios in their Features.

    $ bin/test

You can pass many useful options to this command.
Use `--help` to list them.


Workflow
--------

1. Write a Scenario in the `features/gherkin` directory.
2. Code until `bin/test` is green.
3. Refactor until you're satisfied.


Pitfalls
--------

- It appears that commands such as `bin/rails cucumber` are broken.
  Maybe this can be solved with some `PATH` wiring.

- For some reason, running `script/cucumber` may not work.
  Neither will `bin/rails cucumber`. `BUNDLE_PATH`?

- almost no security whatsoever (add more vigils or features)

- very early stages codebase by rails and ruby noobs
