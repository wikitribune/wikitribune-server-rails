Rails.application.routes.draw do
  # For details on the DSL, see http://guides.rubyonrails.org/routing.html

  # Generated Documentation
  apipie

  ##############################################################################
  # API

  def prefix
    'api/v1/'
  end


  ##############################################################################
  # USERS

  # Setting up User as resource is overkill. Is it?
  # resources :users
  # Making our own routes is more verbose but more zen (for now)
  # match prefix + 'authenticate', to: 'authentication#authenticate', via: :get
  match prefix + 'authenticate', to: 'authentication#authenticate', via: [:get, :post]
  post prefix + 'users', to: 'users#create'


  ##############################################################################
  # ARTICLES

  resources :articles, path: prefix + 'articles'
  match prefix + 'articles/:id/publish', to: 'articles#publish', via: [:post, :patch]
  match prefix + 'articles/:id/diff', to: 'articles#diff', via: [:get]


  ##############################################################################
  # TOPICS

  # FIXME: probable 500s on non-implemented routes (destroy etc.)
  resources :topics, path: prefix + 'topics'

  match prefix + 'classification', to: 'classifications#create', via: [:post, :put]
  match prefix + 'classification', to: 'classifications#destroy', via: [:delete]

end
