# Extending Jbuilder like this allows us to have snake_case properties, methods
# and database columns, while having camelCased keys in the JSON for Schema.org.
# This is meant to be used in `app/views/**/*.json.jbuilder` template files.
class Jbuilder
  def _s2c(snake)
    snake.to_s.camelize(:lower)
  end

  def camel_extract!(object, *attributes)
    if ::Hash === object
      raise("Not implemented (yet).")
      #_extract_hash_values(object, attributes) # perhaps make less snaky too?
    else
      _camel_extract_method_values(object, attributes)
    end
  end

  def _camel_extract_method_values(object, attributes)
    attributes.each{ |key| _set_value _s2c(key), object.public_send(key) }
  end
end