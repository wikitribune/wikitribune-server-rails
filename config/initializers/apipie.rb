Apipie.configure do |config|
  # Since other setters like api_base_url use default_version internally,
  # this goes first.  Note that we're using default_version here as well.
  config.default_version = 'v1'
  # config.version_in_url = 'v1'

  config.app_name     = "Wikitribune REST API WIP"
  config.api_base_url = "/api/" + config.default_version # :(|)
  config.doc_base_url = "/apipie"
  config.app_info     = "
  Modern Journalism.
  WIP.
  Supported :formats: json.
  "

  # Array of ISO_639-1 codes
  # If we provide less than two languages, hyperlinks break
  # eo = Esperanto
  # > "eo is not a valid locale"
  # Interesting read: https://en.wikipedia.org/wiki/List_of_ISO_639-5_codes
  config.languages = ['en', 'es']
  config.default_locale = 'en'

  # where is your API defined?
  # > We need to exclude the concerns to be able to use **.*.rb
  config.api_controllers_matcher = "#{Rails.root}/app/controllers/*.rb"
end

