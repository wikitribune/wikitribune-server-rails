# This singleton wraps JWT to provide token encoding and decoding methods.
# The encode method will be responsible for creating tokens
# based on a payload (user id) and an expiration date.

class JsonWebToken
  # Secret used to encode and decode the token
  # HMAC = Hash-based Message Authentication Code
  # This requires you to set up `config/secrets.yml`.
  HMAC_SECRET = Rails.application.secrets.secret_key_base

  def self.encode(payload, expiration_date = 24.hours.from_now)
    payload[:exp] = expiration_date.to_i
    JWT.encode(payload, HMAC_SECRET)
  end

  def self.decode(token)
    payload, _header = JWT.decode(token, HMAC_SECRET)
    HashWithIndifferentAccess.new payload
  rescue JWT::DecodeError => e
    # Raise a custom error to be handled by our custom handler
    raise ExceptionHandler::InvalidToken, e.message
  end
end