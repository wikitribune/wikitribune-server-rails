class User < ApplicationRecord

  # http://schema.org/Person

  # Encrypt password_digest from password and optional password_confirmation
  has_secure_password

  # Multi-transitive Associations
  # This way we'll be able to have:
  # - authored_articles
  # - curated_articles
  # - starred_articles
  # - etc.
  has_many :authorships
  has_many :authored_articles, through: :authorships, source: :article

  # Validations
  validates :name, presence: true
  validates :password_digest, presence: true
  # Email regex that 99.9% works ; https://emailregex.com
  # Anyhow the best practice is to check email by sending a confirmation code.
  # Perhaps we should not check email with regex at all.
  EMAIL_FORMAT = /\A([\w+\-].?)+@[a-z\d\-]+(\.[a-z]+)*\.[a-z]+\z/i
  validates :email, presence: true, format: { with: EMAIL_FORMAT }

  ##############################################################################

  def published_articles_count
    authored_articles.where.not(date_published: nil).count
  end

  def article_drafts_count
    authored_articles.where(date_published: nil).count
  end

end
