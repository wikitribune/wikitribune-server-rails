class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true

  # To conform with Schema.org, we're using the same names as it does,
  # only we're snake_casing to conform with ActiveRecord.
  class << self
    private

    def timestamp_attributes_for_create
      super << 'date_created'
    end

    def timestamp_attributes_for_update
      super << 'date_modified'
    end
  end
end
