class Topic < ApplicationRecord

  # TODO: look up topics in schema.org

  has_many :classifications
  has_many :articles, through: :classifications, source: :article


  ##############################################################################

  def recent_articles
    articles.order('date_created ASC').reverse_order
    # .limit(p['limit']) \
  end

  ##############################################################################

  def self.find_with_effort!(id)
    got = self.find_by(id: id)
    got = self.find_by(slug: id) unless got
    raise(RecordNotFound.new("Couldn't find #{model_name}", id)) unless got

    got
  end

  def self.create!(params)
    topic = new(params)
    topic.slug = params[:name].to_slug
    topic.save!
    topic
  end
end
