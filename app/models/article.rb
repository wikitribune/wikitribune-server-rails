class Article < ApplicationRecord

  # https://schema.org/Article

  # We don't have to define all the attributes here,
  # since ActiveRecord is smart enough to mirror the database.
  # If you want to know the structure of Articles, see `db/schema.rb`.
  # If you want to edit it, see the migrations in `db/migrate`.

  enum status: [ :suggestion, :draft, :publication ]

  has_many :authorships
  has_many :authors, through: :authorships, source: :user

  has_many :classifications
  has_many :topics, through: :classifications, source: :topic

  # We should probably make a custom validator for the title, eventually,
  # in order to remove whitespaces and emojis before asserting min length.
  validates :headline, presence: true, length: { minimum: 4, maximum: 128 }

  # Let's accept articles with an empty content body, for now.
  # But we should deny articles that are too big, like millions of chars big.
  # validates :article_body, presence: true

  ##############################################################################

  # https://github.com/paper-trail-gem/paper_trail#1c-basic-usage
  has_paper_trail

  ##############################################################################

  ##############################################################################

  # Using the model for logic?
  # This is tempting.

  def publish
    # code smell?
    # we let this return a bool (62% sure), whereas publish! returns self
    update(date_published: Time.new, status: Article.statuses[:publication])
  end

  def publish!
    raise('Cannot publish.') unless publish

    self
  end

  ##############################################################################
  # CLASS METHODS

  def self.find_with_effort(id)
    got = self.find_by(id: id)
    got = self.find_by(slug: id) unless got
    got = self.find_by(uuid: id) unless got

    got # got got
  end

  def self.find_with_effort!(id)
    got = find_with_effort id
    raise(ActiveRecord::RecordNotFound.new("Couldn't find #{model_name}", id)) unless got

    got # got got
  end

  # Factory example ; wip signature.
  def self.create!(params)
    author = params.delete(:author)
    topics = params.delete(:topics)
    article = new(params)
    if params[:headline] and not article.slug
      article.slug = params[:headline].to_slug
    end
    topics&.each do |topic|
      topic = Topic.find_by!(name: topic) if topic.is_a? String
      article.topics << topic
      # TODO: set the author of the categorization
    end
    article.save!
    author.authored_articles << article if author

    article
  end

end
