class AuthenticationController < ApplicationController

  api :GET, '/authenticate.:format', "Authenticate and get a token."
  description "
  You need to go through this to get the JWT (jwt.io) token
  you'll need for most endpoints.
  Best make sure you're using HTTPS when calling this.
  "
  # TODO: use user id instead of email
  param :email, String, '⚠ Temporarily. TODO HELP: use user id (or slug) instead', required: true
  param :password, String, '🔑 We will digest it and compare.', required: true
  # TODO: refactor https://github.com/Apipie/apipie-rails#response-description
  returns code: 200, desc:
      "
      An authentication token you must provide in the 'HTTP_AUTHORIZATION'
      header for subsequent requests requiring an authenticated user.
      " do
    property :auth_token, String, "🔑 Set your 'Authorization' header to this value."
  end

  def authenticate
    auth_token = AuthenticateUser.new(pearl[:email], pearl[:password]).call
    # FIXME: tied to JSON
    json_response(auth_token: auth_token)
  end

  private

  def pearl
    params.permit(:email, :password)
  end
end