class TopicsController < ApplicationController

  # Only authenticated users may use some endpoints.
  before_action :authorize_request, only: %i[classification]

  before_action :set_topic, only: %i[show update destroy]

  QUERY_DEFAULT_LIMIT = 100
  QUERY_MAXIMUM_LIMIT = 200

  ##############################################################################
  # Note: I18N is starting to itch.

  api :GET, '/topics.:format', "List topics."
  description "I18N?"
  param :limit, :number, desc: "A limit between 1 and #{QUERY_DEFAULT_LIMIT}."
  # +offset
  # +JSON-LD
  def index
    # p = { limit: 5 }.merge(query_params) # nope, but atoms would be better.
    p = { 'limit' => QUERY_DEFAULT_LIMIT }.merge(query_params)
    p['limit'] = QUERY_MAXIMUM_LIMIT if p['limit'].to_i <= 0
    p['limit'] = [1, p['limit'].to_i, QUERY_MAXIMUM_LIMIT].sort[1] # clamp

    @topics = Topic.all\
                   .limit(p['limit'])\
                   .order('date_created ASC')\
                   .reverse_order
  end

  ##############################################################################

  api :GET, '/topics/:id.:format', "List most recently published articles of a topic."
  description "/!. The shape of the response may be subject to any change until it's frozen. Help us choose schemas : https://framagit.org/wikitribune/wikitribune-server-rails/issues/4"
  #param :id, :number
  def show
    # Will look for :show view
    # @topic = Topic.find_with_effort!(params[:id])
    @recent_articles = @topic.articles
  end

  ##############################################################################

  private

  def set_topic
    @topic = Topic.find_with_effort!(params[:id])
  end

  def query_params
    params.permit(:limit)
  end
end
