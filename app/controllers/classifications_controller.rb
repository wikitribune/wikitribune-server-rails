class ClassificationsController < ApplicationController
  # Only authenticated users may use some endpoints.
  before_action :authorize_request, only: %i[create destroy]

  before_action :set_topic_and_article, only: %i[create destroy]

  ##############################################################################

  # api :GET, '/topics/:id.:format', "List most recently published articles of a topic."
  # description "/!. The shape of the response may be subject to any change until it's frozen. Help us choose schemas : https://framagit.org/wikitribune/wikitribune-server-rails/issues/4"
  # #param :id, :number
  # def show
  #   # Will look for :show view
  #   # @topic = Topic.find_with_effort!(params[:id])
  #   @recent_articles = @topic.articles
  # end

  ##############################################################################

  def create
    if @topic.articles.where(id: @article.id).count.positive?
      raise ExceptionHandler::InvalidOperation, 'Article already has topic.'
    end

    @topic.articles << @article

    respond_to do |format|
      # FIXME: Return a Classification
      format.json { render json: 'ok', status: :ok }
    end
  end

  def destroy
    if @topic.articles.where(id: @article.id).count.zero?
      raise ExceptionHandler::InvalidOperation, 'Article does not have topic.'
    end

    c = Classification.find_by(article_id: @article.id, topic_id: @topic.id)
    c.destroy

    respond_to do |format|
      # FIXME: Return what?
      format.json { render json: 'ok', status: :ok }
    end
  end

  ##############################################################################

  private

  def set_topic_and_article
    @topic = Topic.find_with_effort!(params[:topic])
    @article = Article.find_with_effort!(params[:article])
  end
end
