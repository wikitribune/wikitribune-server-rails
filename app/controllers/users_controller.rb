class UsersController < ApplicationController

  # Should we care about them being Persons?
  # https://schema.org/Person

  api :POST, '/users.:format', "Register in the ledger of users."
  description "
  Best make sure you're using HTTPS when calling this.
  We'll provide you stamped letters for your articles,
  but you have to find your own quill, ink and paper."
  # https://schema.org/name
  param :name, String, 'Eg: "John Doe".  Choose wisely.  We recommend using your real name.', required: true
  # https://schema.org/email
  param :email, String, 'Your email will not be shared.'

  param :password, String, 'Your password will not be stored as-is.  We will store a digested password.', required: true

  # returns code: 200, desc: "
  #   An authentication token you must provide in the Authorization header
  #   for subsequent requests requiring an authenticated user." do
  #   property :auth_token, String, "🔑 Set your authorization header (`HTTP_AUTHORIZATION`) to this value."
  # end

  def create
    @user = User.create!(user_params.merge(slug: user_params[:name].to_slug))
    @auth_token = AuthenticateUser.new(@user.email, @user.password).call

    render :show, status: :ok
  end

  ##############################################################################

  private

  def user_params
    params.permit(
      :name,
      :email,
      :password
      # :password_confirmation
    )
  end

end
