class ArticlesController < ApplicationController

  # We can maybe drop the HTML version of the API.
  # We should support XML (schema.org!), YML and TXT.
  # We should perhaps use Content-Type headers instead of extensions?
  # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Content-Type
  # Rails appears to rely on extensions, so maybe we should play along.

  ##############################################################################

  # Only authenticated users may use these endpoints.
  before_action :authorize_request, except: %i[index show]

  ##############################################################################

  before_action :set_article, only: %i[show update destroy publish diff]

  ##############################################################################

  before_action :set_paper_trail_whodunnit

  ##############################################################################

  QUERY_DEFAULT_LIMIT = 5
  QUERY_MAXIMUM_LIMIT = 100

  ##############################################################################

  # TBD in Gherkin
  # What's a good behavior, there? Pagination is a classic.
  # What about incorrect parameters? Should we ignore and serve our best,
  # or yell at the API user?
  # Perhaps we need a HTTP header or param to request using a stricter mode.
  # Or the opposite, be strict by default and add an option for leniency?
  api :GET, '/articles.:format', "List most recently published articles."
  description "perhaps make this more static in nature when there's no query parameters?"
  param :limit, :number, desc: "A limit between 1 and #{QUERY_DEFAULT_LIMIT}."
  def index
    # p = { limit: 5 }.merge(query_params) # nope, but atoms would be better.
    p = { "limit" => QUERY_DEFAULT_LIMIT }.merge(query_params)
    p['limit'] = QUERY_MAXIMUM_LIMIT if p['limit'].to_i <= 0
    p['limit'] = [1, p['limit'].to_i, QUERY_MAXIMUM_LIMIT].sort[1] # clamp

    @articles = Article.all\
                       .limit(p['limit'])\
                       .order('date_created ASC')\
                       .reverse_order
  end

  api :GET, '/articles/:id.:format', "Find an article from its id."
  description "/!. The shape of the response may be subject to any change until it's frozen. Help us choose schemas : https://framagit.org/wikitribune/wikitribune-server-rails/issues/4"
  #param :id, :number
  def show
    # Will look for eponym view
  end

  # GET /articles/new
  # def new
  #   @article = Article.new
  # end

  # GET /articles/1/edit
  # def edit
  # end

  api :POST, '/articles.:format', "Submit a new article draft."
  param :headline, String, desc: "A title (not too short)"
  param :article_body, String, desc: "Text contents of the article"
  def create
    @article = Article.create!(article_params.merge(author: current_user))

    if params[:suggestion]&.param_to_bool
      @article.status = Article.statuses[:suggestion]
      @article.save!
    end

    respond_to do |format|
      if @article
        format.html { redirect_to @article, notice: 'Article was successfully created.' }
        format.json { render :show, status: :created, location: @article }
      else
        format.html { render :new }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  api :PUT, '/articles/:id.:format', "Update an article with new contents."
  param :id, :number
  param :headline, String, desc: "Be very cautious when updating the title of an article.  It may change its URL and break existing hyperlinks."
  param :article_body, String, desc: "Updated contents of the article."
  def update
    respond_to do |format|
      if @article.update(article_params)
        format.html { redirect_to @article, notice: 'Article was successfully updated.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /articles/1
  # DELETE /articles/1.json
  # def destroy
  #   @article.destroy
  #   respond_to do |format|
  #     format.html { redirect_to articles_url, notice: 'Article was successfully destroyed.' }
  #     format.json { head :no_content }
  #   end
  # end

  api :PATCH, '/articles/:id/publish.:format', "Publish an article."
  description "
  This operation should be irreversible.
  "
  param :id, :number
  def publish
    respond_to do |format|
      if @article.publish
        format.html { redirect_to @article, notice: 'Article was successfully published.' }
        format.json { render :show, status: :ok, location: @article }
      else
        format.html { render :edit }
        format.json { render json: @article.errors, status: :unprocessable_entity }
      end
    end
  end


  api :GET, '/articles/:id/diff.:format', "Find the diff with the previous version."
  description "Not sure if relevant as-is."
  param :id, :number
  def diff
    chgs = @article.versions.last.changeset
    diff = Diffy::Diff.new(chgs[:article_body][0], chgs[:article_body][1])
    respond_to do |format|
      format.json { render json: diff, status: :ok }
    end
  end

  ##############################################################################
  ##############################################################################

  private

  def set_article
    @article = Article.find_with_effort!(params[:id])
  end

  # Never trust parameters from the scary internet,
  # only allow the white lists below through.

  def article_params
    # No sure we want our params under the :article "namespace".
    # params.fetch(:article, {}).permit(:title)
    # Let's do this the old-fashioned way instead ; change my mind.
    params.permit(:headline, :article_body)
  end

  def query_params
    params.permit(:limit)
  end

end
