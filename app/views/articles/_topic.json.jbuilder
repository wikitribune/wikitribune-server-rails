# Here we also go from snake_case to camelCase, justin case.
json.camel_extract! topic, \
    :id, \
    :slug, \
    :name
