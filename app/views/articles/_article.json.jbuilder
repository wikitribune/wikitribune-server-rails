# Eternal Todo
# Add as much of http://schema.org/Article as we can.
# That means camelCased keys!
# Example:
# json.articleBody = "Hello world"

# Here we also go from snake_case to camelCase.
json.camel_extract! article, \
    :id, \
    :slug, \
    :date_created, :date_modified, :date_published, \
    :headline, :article_body
json.status article.status
json.topics article.topics do |topic|
  json.partial! "articles/topic", topic: topic
end

# Not 100% sure about how this works, nor why the User hasn't it.
json.url article_url(article, format: :json)
