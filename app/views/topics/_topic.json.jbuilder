json.camel_extract! topic, :id, :slug, :name, :date_created, :date_modified
# json.url topic_url(topic, format: :json)

json.articles do
  json.all(topic.articles) do |article|
    # TODO Linked data, here
    json.id article.id
    json.headline article.headline
  end

  json.recent(topic.recent_articles) do |article|
    # TODO Linked data, here
    json.id article.id
    json.headline article.headline
  end
end
